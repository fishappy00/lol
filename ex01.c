#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

struct thread_args 
{
	int arg_num;
	char* arg_arr[];
};

void *print_avg(void *args)
{
	int avg=0;
	struct thread_args *thread_args =(struct thread_args*) args;
	int arr_num = thread_args->arg_num;

	for(int i = 1; i <arr_num; i++){
		avg += atoi(thread_args->arg_arr[i]);
	}
	avg = avg/arr_num;
	printf("\nAverage is %d",&avg);
}

void *print_largest(void *args)
{
	struct thread_args *thread_args =(struct thread_args*) args;
	int largest = atoi(thread_args-> arg_arr[1]);
	int arr_num = thread_args-> arg_num;
	for(int i = 2; i < arr_num; i++){
		if( largest < atoi(thread_args->arg_arr[i])) largest = atoi(thread_args->arg_arr[i]);
	}
	printf("\nLargest is %d", &largest);

}

void *print_smallest(void *args)
{
	struct thread_args *thread_args = (struct thread_args*)args;
	int smallest = atoi(thread_args-> arg_arr[1]);
	int arr_num = thread_args -> arg_num;
	printf("smallest's len: %d", &arr_num);
	for(int i = 2; i <arr_num; i++) {
		if( smallest > atoi(thread_args->arg_arr[i])) smallest = atoi(thread_args->arg_arr[i]);
		printf("\nsmallest: %d",&smallest);
	}
	printf("\nSmallest is %d", &smallest);
}

int main(int argn, char* args[])
{
	pthread_t tid[3];
	struct thread_args *thread_args = malloc(sizeof(struct thread_args)+argn*sizeof(char*));
	thread_args->arg_num = argn;
	for(int i = 0; i <argn;i++){
		thread_args->arg_arr[i] =malloc(strlen(args[i])+1);
		strcpy(thread_args->arg_arr[i], args[i]);
	}

	if(pthread_create(&tid[0], NULL, &print_avg, &thread_args))
	{
		printf("Error creating the first thread! Exiting...");
		return 0;
	}

	if(pthread_create(&tid[1], NULL, &print_largest, &thread_args))
	{
		printf("Error creating the second thread! Exiting...");
		return 0;
	}

	if(pthread_create(&tid[2], NULL, &print_smallest, &thread_args))
	{
		printf("Error creating the second thread! Exiting...");
		return 0;
	}
	sleep(5);
	return 0;
}
